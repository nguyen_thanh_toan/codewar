package com.gmm.bot.model;

import com.gmm.bot.enumeration.GemType;
import com.gmm.bot.enumeration.HeroIdEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.stream.Collectors;

@Setter
@Getter
public class Player {
    private int id;
    private String displayName;
    private List<Hero> heroes;
    private Set<GemType> heroGemType;
    public String targetHeroBuff = "MONK";

    public Player(int id, String displayName) {
        this.id = id;
        this.displayName = displayName;
        heroes = new ArrayList<>();
        heroGemType = new LinkedHashSet<>();
    }

    public Optional<Hero> anyHeroFullMana() {
        return heroes.stream().filter(hero -> hero.isAlive() && hero.isFullMana() && !hero.getId().equals(HeroIdEnum.ELIZAH)).findFirst();
    }
    public List<Hero> anyHeroDead() {
        return heroes.stream().filter(hero -> !hero.isAlive()).collect(Collectors.toList());
    }

    public boolean heroLowHealth(int atk) {
        Hero h = firstHeroAlive();


        return h.getHp() <= atk;
    }

    public boolean ShouldUseSword(){
        List<Hero> heroList = anyHeroDead();
        int count = 0;
        for (Hero hero: heroList) {
            System.out.println(hero.getName());
            if (Objects.equals(hero.getName(), "FIRE_SPIRIT") || Objects.equals(hero.getName(), "CERBERUS"))
                count++;
        }
        return count >= 2;
    }

    public Hero HeroHighestHP(){
        Hero h = firstHeroAlive();
        for (Hero hero: heroes) {
            if (hero.getHp() <= h.getHp() && hero.isAlive())
                h = hero;
        }

        return h;
    }
    public Hero firstHeroAlive() {
        return heroes.stream().filter(Hero::isAlive).findFirst().orElse(null);
    }

    public Set<GemType> getRecommendGemType() {
        heroGemType.clear();
        heroes.stream().filter(Hero::isAlive).forEach(hero -> heroGemType.addAll(hero.getGemTypes()));

        return heroGemType;
    }
}
